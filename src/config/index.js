if(process.env.NODE_ENV!="production"){
    require('dotenv').config()
}

module.exports={
    PORT:process.env.PORT,
    APP_NAME:process.env.APPLICATION_NAME,
    JWT_SECRET:process.env.JWT_SECRET,
    MONGO_URI:process.env.MONGO_URI,
}