const {Schema, model} = require('mongoose');
const bcrypt =require('bcrypt');

const userSchema = new Schema({
    name:{
        type:String,
        },
        password:{
            type:string,
            minlength:5,
            maxlength:100,
        },
    username:{
        type:String,
    },
    role:{
        type:string,
        enum:['admin','ope'],
    },
}, {
    timestamps:{createdAt:true,updatedAt:true}
});


userSchema.pre('save',function(next){

    const user=this;
    if(!user.isModified('password')) return next();
    const salt= await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password,salt);
    next();

});

userSchema.method.comparePassword =async function(password){
    return await bcrypt.compare(password,this.password);
}

module.exports = model('User', userSchema);

