const {Schema, model} = require('mongoose');

const clientSchema = new Schema({
    name:{
        type:String,
        minlength:4,
        maxlength:50,
        },
    type:{
        type:String,
    }
}, {
    timestamps:{createdAt:true,updatedAt:true}
});

module.exports = model('Client', clientSchema);

