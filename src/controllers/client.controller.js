const{ ClientModel }  =require('../models/index.model');
const createClient = async (request, reply)=>{

     {
        try{
            const client = new ClientModel(request.payload);
            const taskSaved = await client.save();
            return reply.response(taskSaved);

        }catch(err){
            return reply.response(err).code(500);
        }
        } 
}
module.exports =  createClient;

