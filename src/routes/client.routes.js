const { ClientRoutes } = require('./index.routes');

const {Server} =require('@hapi/hapi');
const Joi = require('@hapi/joi');
const {ClientController}=require('../controllers/index.controller')

const routes=(server) => {
    server.route({
        path:'/client',
        method:'POST',
        options:{
        validate:{
            payload:Joi.object({
                name:Joi.string().required().min(5),
                type:Joi.string(),
            },),
            failAction:(request,reply,err)=>{
                return err.isJoi ? reply.response(err.details[0]).takeover():reply.response(err).takeover()
            }
        }},
        handler:ClientController
    })

    server.route({
        path:'/client',
        method:'GET',
        handler: async (request,reply) => {
        try{
            
            const clients = await ClientModel.find();
            return reply.response(clients);

        }catch(err){
            return reply.response(err).code(500);
        }
        }
    })

    server.route({
        path:'/client/{id}',
        method:'GET',
        handler: async (request,reply) => {
        try{
            const id = request.params.id;
            const client = await ClientModel.find(id);
            return reply.response(client);

        }catch(err){
            return reply.response(err).code(500);
        }
        }
    })

    
    server.route({
        path:'/client/{id}',
        method:'PUT',
        handler: async (request,reply) => {
        try{
            const id = request.params.id;
            const newInfo = (request.payload);
            const client = await ClientModel.findByIdAndUpdate(id,newInfo,{new:true});

            return reply.response(client);

        }catch(err){
            return reply.response(err).code(500);
        }
        }
    })
};
module.exports = routes;