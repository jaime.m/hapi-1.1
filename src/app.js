const Hapi = require('@hapi/hapi');
const { PORT } =require('./config')
console.log(PORT);
const port = PORT || 3000;
require('./database');
const { ClientRoutes } =require('./routes/index.routes');

const init = async ()=>{
    const server = new Hapi.Server({
        host:'localhost',
        port:port,
    });

    ClientRoutes(server);

    await server.start();
    
    console.log(`server working ar ${server.info.uri}` );
    //Add some routes to server request
    server.route({
        path:'/',
        method:'GET',
        handler:(request,reply)=>{
            return ('hello! Hapi working');
        }
    });

    
};

init();


